﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WmsVertereProjetoPadraoApi.Models.TipoProduto;

namespace WmsVertereProjetoPadraoApi.Repository.TipoProduto
{
    public interface ITipoProdutoRepository
    {
        string TipoProdutoAdd(List<TipoProdutoModel> tipos);
        string TipoProdutoUpd(List<TipoProdutoModel> tipos);
        string TipoProdutoDel(List<TipoProdutoModel> tipos);
        IEnumerable<TipoProdutoModel> TipoProdutoGetTodos();
        IEnumerable<TipoProdutoModel> TipoProdutoGetById(int idTipoProduto);
    }
}
