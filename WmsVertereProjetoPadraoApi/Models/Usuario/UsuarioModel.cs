﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WmsVertereProjetoPadraoApi.Models.Usuario
{
    public class UsuarioModel : IDisposable
    {
        public int ID { get; set; }
        public string NOME { get; set; }
        public string EMAIL { get; set; }
        public string SENHA { get; set; }
        public string IDEXTERNO { get; set; }
        public string DTADD { get; set; }
        public string USERADD { get; set; }
        public string DTUPD { get; set; }
        public string USERUPD { get; set; }
        public string DTDEL { get; set; }
        public string USERDEL { get; set; }
        public string USUARIO { get; set; }
        public string SALT { get; set; }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                { }

                disposedValue = true;
            }
        }

        ~UsuarioModel()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
