﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WmsVertereProjetoPadraoApi.Models
{
    public class ExempleModel
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public int Idade { get; set; }
    }
}
