﻿using System;

namespace WmsVertereProjetoPadraoApi.Models.TipoProduto
{
    public class TipoProdutoModel : IDisposable
    {
        public int ID { get; set; }
        public string DESCRICAO { get; set; }
        public double PESO { get; set; }
        public string DTADD { get; set; }
        public string USERADD { get; set; }
        public string DTUPD { get; set; }
        public string USERUPD { get; set; }
        public string DTDEL { get; set; }
        public string USERDEL { get; set; }
        public string USUARIO { get; set; }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                { }

                disposedValue = true;
            }
        }

        ~TipoProdutoModel()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
