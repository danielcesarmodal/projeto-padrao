﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WmsVertereProjetoPadraoApi.Models.TipoProduto;

namespace WmsVertereProjetoPadraoApi.DbScripts.TipoProduto
{
    public class TipoProdutoDbScript : IDisposable
    {
        public TipoProdutoDbScript() {Dispose(true); }
        
        public Dictionary<string,object> TipoProdutoAdd(TipoProdutoModel tipo) {            
            string SQL = "INSERT INTO TIPOPRODUTO (DESCRICAO, PESO, DTADD, USERADD, ATIVO) VALUES (:DESCRICAO, :PESO, SYSTIMESTAMP, :USUARIO, 1)";                            
            return new Dictionary<string, object>() { { SQL, tipo } }; 
        }
 
        public Dictionary<string, object> TipoProdutoUpd(TipoProdutoModel tipo)
        {
            string SQL = "UPDATE TIPOPRODUTO SET DESCRICAO = :DESCRICAO, PESO = :PESO, DTUPD = SYSTIMESTAMP,USERUPD = :USUARIO WHERE ID = :ID";            
            return new Dictionary<string, object>() { { SQL, tipo } };
        }

        public Dictionary<string, object> TipoProdutoDel(TipoProdutoModel tipo)
        {           
            string SQL = "UPDATE TIPOPRODUTO SET ATIVO = 2, DTDEL = SYSTIMESTAMP, USERDEL = :USUARIO WHERE ID = :ID";         
            return new Dictionary<string, object>() { { SQL, tipo } };
        }

        public string TipoProdutoGetTodos()
        {
            string SQL = "SELECT ID, DESCRICAO, PESO, DTADD, USERADD, DTUPD, USERUPD, DTDEL, USERDEL USUARIO FROM TIPOPRODUTO WHERE ATIVO = 1 ORDER BY ID DESC";
            return SQL;
        }

        public Dictionary<string, object> TipoProdutoGetById(int _idTipoProduto)
        {
            string SQL = "SELECT ID, DESCRICAO, PESO FROM TIPOPRODUTO WHERE ATIVO = 1 AND ID = :IDTIPOPRODUTO";
            return new Dictionary<string, object>() { { SQL, new { IDTIPOPRODUTO = _idTipoProduto } } };
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                { }

                disposedValue = true;
            }
        }

        ~TipoProdutoDbScript()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
