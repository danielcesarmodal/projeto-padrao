﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using WmsVertereProjetoPadraoApi.Common;
using WmsVertereProjetoPadraoApi.Models.TipoProduto;
using WmsVertereProjetoPadraoApi.Repository.TipoProduto;


namespace WmsVertereProjetoPadraoApi.Controllers.TipoProduto
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsAllowAll")]
    public class TipoProdutoController : ControllerBase
    {
        private readonly ITipoProdutoRepository _tipoProdutoRepository;
        ValidaUser validaUser = new ValidaUser();
        private StringValues token;
        private StringValues user;

        public TipoProdutoController(ITipoProdutoRepository tipoProdutoRepository)
        {
            _tipoProdutoRepository = tipoProdutoRepository;
        }

        /// <summary>
        /// Cria Tipos Produtos.
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="511">Usuário não autenticado</response>
        /// <response code="500">Erro no método</response>
        [HttpPost]
        public IActionResult TipoProdutoAdd(List<TipoProdutoModel> tipos)
        {
            try
            {
                Request.Headers.TryGetValue("Authorization", out token);
                Request.Headers.TryGetValue("User", out user);
                if (!validaUser.ValidarUser(token.FirstOrDefault()))
                {
                    validaUser.Dispose();
                    new MyLog().GerarLog("TipoProdutoAdd", "Usuário não autenticado. Usuário: " + user.FirstOrDefault());
                    return BadRequest(new Error(HttpStatusCode.NetworkAuthenticationRequired, "TipoProdutoAdd", "Usuário não autenticado"));
                }
                 return(new UtilController().verificaStatus(_tipoProdutoRepository.TipoProdutoAdd(tipos)));
            }
            catch (Exception ex)
            {
                new MyLog().GerarLog("TipoProdutoAdd", "Erro ao gravar tipos produtos", ex);
                return BadRequest(new Error(HttpStatusCode.InternalServerError, "TipoProdutoAdd", ex.Message));
            }
        }

        /// <summary>
        /// Atualiza Tipos Produtos.
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="511">Usuário não autenticado</response>
        /// <response code="500">Erro no método</response>
        [HttpPut]
        public IActionResult TipoProdutoUpd(List<TipoProdutoModel> tipos)
        {
            try
            {
                Request.Headers.TryGetValue("Authorization", out token);
                Request.Headers.TryGetValue("User", out user);
                if (!validaUser.ValidarUser(token.FirstOrDefault()))
                {
                    validaUser.Dispose();
                    new MyLog().GerarLog("TipoProdutoUpd", "Usuário não autenticado. Usuário: " + user.FirstOrDefault());
                    return BadRequest(new Error(HttpStatusCode.NetworkAuthenticationRequired, "TipoProdutoUpd", "Usuário não autenticado"));
                }
                 return(new UtilController().verificaStatus(_tipoProdutoRepository.TipoProdutoUpd(tipos)));
            }
            catch (Exception ex)
            {
                new MyLog().GerarLog("TipoProdutoUpd", "Erro ao atualizar tipos produtos", ex);
                return BadRequest(new Error(HttpStatusCode.InternalServerError, "TipoProdutoUpd", ex.Message));
            }
        }

        /// <summary>
        /// Deleta Tipos Produtos.
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        /// <response code="511">Usuário não autenticado</response>
        /// <response code="500">Erro no método</response>
        [HttpDelete]
        public IActionResult TipoProdutoDel(List<TipoProdutoModel> tipos)
        {
            try
            {
                Request.Headers.TryGetValue("Authorization", out token);
                Request.Headers.TryGetValue("User", out user);
                if (!validaUser.ValidarUser(token.FirstOrDefault()))
                {
                    validaUser.Dispose();
                    new MyLog().GerarLog("TipoProdutoDel", "Usuário não autenticado. Usuário: " + user.FirstOrDefault());
                    return BadRequest(new Error(HttpStatusCode.NetworkAuthenticationRequired, "TipoProdutoDel", "Usuário não autenticado"));
                }
                 return(new UtilController().verificaStatus(_tipoProdutoRepository.TipoProdutoDel(tipos)));
            }
            catch (Exception ex)
            {
                new MyLog().GerarLog("TipoProdutoDel", "Erro ao deletar tipos produtos", ex);
                return BadRequest(new Error(HttpStatusCode.InternalServerError, "TipoProdutoDel", ex.Message));
            }
        }

        /// <summary>
        /// Lista todos os tipos produto.
        /// </summary>
        /// <returns>Tipo Produto</returns>
        /// <response code="200">Returna os tipos produto cadastrados</response>
        /// <response code="511">Usuário não autenticado</response>
        /// <response code="500">Erro no método</response>
        [HttpGet]
        public IActionResult TipoProdutoGetTodos()
        {
            try
            {
                Request.Headers.TryGetValue("Authorization", out token);
                if (!validaUser.ValidarUser(token.FirstOrDefault()))
                {
                    validaUser.Dispose();
                    new MyLog().GerarLog("TipoProdutoGetTodos", "Usuário não autenticado - " + token.FirstOrDefault());
                    return BadRequest(new Error(HttpStatusCode.NetworkAuthenticationRequired, "TipoProdutoGetTodos", "Usuário não autenticado"));
                }
                return Ok(_tipoProdutoRepository.TipoProdutoGetTodos());
            }
            catch (Exception ex)
            {
                new MyLog().GerarLog("TipoProdutoGetTodas", "Erro ao buscar todas os tipos produtos", ex);
                return BadRequest(new Error(HttpStatusCode.InternalServerError, "TipoProdutoGetTodas", ex.Message));
            }
        }

        /// <summary>
        /// Busca o tipo produto por Id.
        /// </summary>
        /// <returns>Tipo Produto</returns>
        /// <response code="200">Returna o tipo produto cadastrado por Id</response>
        /// <response code="511">Usuário não autenticado</response>
        /// <response code="500">Erro no método</response>
        [HttpGet("{id}")]
        public IActionResult TipoProdutoGetById(int id)
        {
            try
            {
                Request.Headers.TryGetValue("Authorization", out token);
                if (!validaUser.ValidarUser(token.FirstOrDefault()))
                {
                    validaUser.Dispose();
                    new MyLog().GerarLog("TipoProdutoGetById", "Usuário não autenticado - " + token.FirstOrDefault());
                    return BadRequest(new Error(HttpStatusCode.NetworkAuthenticationRequired, "TipoProdutoGetById", "Usuário não autenticado"));
                }
                return Ok(_tipoProdutoRepository.TipoProdutoGetById(id));
            }
            catch (Exception ex)
            {
                new MyLog().GerarLog("TipoProdutoGetById", "Erro ao buscar tipo produto por Id", ex);
                return BadRequest(new Error(HttpStatusCode.InternalServerError, "TipoProdutoGetById", ex.Message));
            }
        }
    }
}