﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WmsVertereProjetoPadraoApi.Common
{
    public class UtilController : ControllerBase
    {
        public IActionResult verificaStatus(string retorno)
        {
            if (retorno == "OK")
            {
                return Ok(retorno);
            }
            else
            {
                return BadRequest("Erro : " + retorno);
            }
        }
    }
}
